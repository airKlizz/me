function withOpacityValue(variable) {
  return ({ opacityValue }) => {
    if (opacityValue === undefined) {
      return `rgb(var(${variable}))`;
    }
    return `rgb(var(${variable}) / ${opacityValue})`;
  };
}

function value(variable) {
  return () => {
    return `var(${variable})`;
  };
}

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      spacing: {
        theme: value("--spacing"),
        border: value("--spacing-border"),
      },
      borderRadius: {
        "theme-ext": value("--border-radius-ext"),
        "theme-int": value("--border-radius-int"),
      },
      colors: {
        text: withOpacityValue("--color-text"),
        "bg": withOpacityValue("--color-bg"),
        "bg-from": withOpacityValue("--color-bg-from"),
        "bg-via": withOpacityValue("--color-bg-via"),
        "bg-to": withOpacityValue("--color-bg-to"),
        primary: withOpacityValue("--color-primary"),
        secondary: withOpacityValue("--color-secondary"),
      },
      aspectRatio: {
        "10/4": "10 / 4",
        "4/3": "4 / 3",
        "2/1": "2 / 1",
      },
    },
  },
  plugins: [],
};
