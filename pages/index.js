import Grid from "../components/Grid";
import React, { useState, useEffect } from "react";
import Head from "next/head";

export default function Home() {
  const [style, setStyle] = useState("theme-elegant");
  const [light, setLight] = useState("theme-light");

  useEffect(() => {
    document.querySelector("#main").classList.add(style);
    document.querySelector("#main").classList.add(light);
  });

  function inverseStyle() {
    switch (style) {
      case "theme-default":
        document.querySelector("#main").classList.remove("theme-default");
        setStyle("theme-elegant");
        break;
      default:
        document.querySelector("#main").classList.remove("theme-elegant");
        setStyle("theme-default");
        break;
    }
  }

  function inverseLight() {
    switch (light) {
      case "theme-light":
        document.querySelector("#main").classList.remove("theme-light");
        setLight("theme-dark");
        break;
      default:
        document.querySelector("#main").classList.remove("theme-dark");
        setLight("theme-light");
        break;
    }
  }

  return (
    <>
      <Head>
        <title>Rémi Calizzano - Curriculum Vitae</title>
        <meta
          property="og:title"
          name="title"
          content="Rémi Calizzano - Curriculum Vitae"
          key="title"
        />
        <meta
          property="og:description"
          name="description"
          content="Webpage to present Rémi Calizzano's curriculum vitae"
          key="description"
        />
      </Head>
      <main
        id="main"
        className="bg-gradient-to-br from-bg-from via-bg-via to-bg-to
    text-text"
      >
        <div className="container max-w-screen-lg mx-auto py-6 px-theme">
          <div className="flex mb-6 gap-2">
            <h1 className="p-1 font-mono text-2xl font-semibold capitalize mr-auto">
              curriculum vitae
            </h1>
            <button className="p-1 text-2xl" onClick={inverseLight}>
              {light == "theme-light" ? "🌚" : "🌞"}
            </button>
            <button className="p-1 text-2xl" onClick={inverseStyle}>
              {style == "theme-default" ? "🤓" : "😎"}
            </button>
          </div>
          <Grid />
        </div>
      </main>
    </>
  );
}
