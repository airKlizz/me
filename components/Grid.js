import Header from "./GridItems/Header";
import Avatar from "./GridItems/Avatar";
import Social from "./GridItems/Social";
import Skills from "./GridItems/Skills";
import Jobs from "./GridItems/Jobs";
import Projects from "./GridItems/Projects";
import Publications from "./GridItems/Publications";
import Education from "./GridItems/Education";
import Contact from "./GridItems/Contact";

export default function Grid() {
  return (
    <div className="grid grid-cols-2 lg:grid-cols-12 gap-theme">
      <Header />
      <Avatar />
      <Social />
      <Skills />
      <Jobs />
      <Projects />
      <Education />
      <Publications />
      <Contact />
    </div>
  );
}
