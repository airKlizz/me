import { Title } from "./Utils/Title";

export default function Header() {
  return (
    <div className="header p-border rounded-theme-ext col-span-2 lg:col-span-10 lg:row-span-4 bg-gradient-to-br from-primary to-secondary">
      <div className="p-3 h-full w-full bg-bg/75 rounded-theme-int">
        <Title>Rémi Calizzano</Title>
        <p>
          Graduated from INSA Lyon Telecom department in September 2020, my
          engineering training was focused on networks and computer science. I
          did several projects around AI (mainly NLP) which led me to do my end
          of study internship at DFKI Berlin.
        </p>
        <p className="mt-2">
          During this internship I was able to continue my work on NLP,
          implement AI solutions as part of the QURATOR project, and above all I
          discovered the research world.
        </p>
        <p className="mt-2">
          After this internship, I continued to work at DFKI as a software
          engineer on a different project: the European Language Grid (ELG).
          This European project is developing a platform for language
          technologies in Europe. While continuing the research, this project
          also introduced me to the administration of micro-services
          infrastructures, a passion that I am now deepening with my work, but
          also through side-projects.
        </p>
      </div>
    </div>
  );
}
