import { Title, SubTitle } from "./Utils/Title";

function School({ school, department, year, description, href }) {
  return (
    <li className="grid grid-cols-6 gap-2">
      <div className="col-span-5">
        <a target="_blank" rel="noopener noreferrer" href={href} className="hover:underline hover:cursor-pointer">
          <p className="inline font-semibold ">{school},</p>
          <p className="inline italic "> {department}.</p>
        </a>
        <p className="text-sm">{description}</p>
      </div>
      <div>
        <p className="text-right">{year}</p>
      </div>
    </li>
  );
}

export default function Education() {
  return (
    <div className="education p-border rounded-theme-ext col-span-2 lg:col-span-4 lg:row-span-6 bg-gradient-to-br from-primary to-secondary">
      <div className="p-3 h-full w-full bg-bg/75 rounded-theme-int">
        <Title>Education</Title>
        <ul className="space-y-4">
          <School
            school="INSA Lyon"
            department="Department of Telecommunications, Services and Uses, Lyon, France"
            href="https://telecom.insa-lyon.fr/en/"
            year="2020 2017"
            description="Engineer school: telecommunications network, computer science"
          />
          <School
            school="UiS"
            department="Departement of computer science, Stavanger, Norway"
            href="https://www.uis.no/en/department-electrical-engineering-and-computer-science"
            year="2019"
            description="University exchange: machine learning, deep learning, nlp"
          />
          <School
            school="INSA Lyon"
            department="Preparatory class, Lyon, France"
            href="https://www.insa-lyon.fr/en/"
            year="2017 2015"
            description="Preparatory for engineering schools: math, physics, chemistry"
          />
          <School
            school="High school"
            department="Albert Camus, Rillieux-La-Pape, France"
            href="https://camus-sermenaz-rillieux.ent.auvergnerhonealpes.fr/"
            year="2015"
            description="Graduate of the Baccalaureate with honors"
          />
        </ul>
      </div>
    </div>
  );
}
