import Link from "next/link";
import { Title, SubTitle } from "./Utils/Title";
import Image from "next/image"


function CategoryUl({ children }) {
  return <ul className="flex flex-wrap gap-x-2 gap-y-1">{children}</ul>;
}

function Techo({ src, imgClassName, name, href }) {
  return (
    <li>
      <a target="_blank" rel="noopener noreferrer" href={href}>
        <div className="flex items-center gap-x-1 rounded-lg px-1 border-2 border-text bg-bg/50">
          {src != null && (
            <Image
              className={`h-5 ${imgClassName != null ? imgClassName : ""}`}
              src={src}
              alt={`${name} logo`}
              width={22}
              height={22}
            />
          )}
          <p className={src != null ? "border-l-2 pl-1 border-text" : ""}>
            {name}
          </p>
        </div>
      </a>
    </li>
  );
}

export default function Skills() {
  return (
    <div className="skills p-border rounded-theme-ext col-span-2 lg:col-span-4 lg:row-span-6 bg-gradient-to-br from-primary to-secondary">
      <div className="p-3 rounded-theme-int w-full h-full bg-bg/75">
        <Title>Skills</Title>
        <SubTitle>ML/NLP</SubTitle>
        <CategoryUl>
          <Techo
            src="/logos/huggingface_logo-noborder.svg"
            name="HuggingFace"
            href="https://huggingface.co/"
          />
          <Techo
            src="/logos/PyTorch_logo_icon.svg"
            name="Pytorch"
            href="https://pytorch.org/"
          />
          <Techo
            src="/logos/pandas_mark.svg"
            name="Pandas"
            href="https://pandas.pydata.org/"
          />
          <Techo name="NLTK" href="https://www.nltk.org/" />
          <Techo
            src="/logos/Python-logo-notext.svg"
            name="Python"
            href="https://www.python.org/"
          />
        </CategoryUl>
        <SubTitle>DevOps</SubTitle>
        <CategoryUl>
          <Techo
            src="/logos/docker.svg"
            name="Docker"
            href="https://www.docker.com/"
          />
          <Techo
            src="/logos/kubernetes-icon-color.svg"
            name="Kubernetes"
            href="https://kubernetes.io/"
          />
          <Techo
            src="/logos/flux-icon-color.svg"
            name="Flux"
            href="https://fluxcd.io/"
          />
          <Techo
            src="/logos/gitlab-logo-500.svg"
            imgClassName="scale-[1.8]"
            name="GitLab"
            href="https://about.gitlab.com/"
          />
          <Techo
            src="/logos/Go-Logo_Aqua.svg"
            name="Go"
            href="https://go.dev/"
          />
        </CategoryUl>
        <SubTitle>Web</SubTitle>
        <CategoryUl>
          <Techo
            src="/logos/fastapi.svg"
            name="FastAPI"
            href="https://fastapi.tiangolo.com/"
          />
          <Techo
            src="/logos/next-js-logo-8FCFF51DD2-seeklogo.com.png"
            name="NextJS"
            href="https://nextjs.org/"
          />
          <Techo
            src="/logos/React-icon.svg"
            name="React"
            href="https://reactjs.org/"
          />
          <Techo
            src="/logos/HTML5_Badge.svg"
            name="HTML"
            href="https://developer.mozilla.org/en-US/docs/Web/HTML"
          />
          <Techo
            src="/logos/CSS3_logo.svg"
            name="CSS"
            href="https://developer.mozilla.org/en-US/docs/Web/CSS"
          />
        </CategoryUl>
        <SubTitle>Tools</SubTitle>
        <CategoryUl>
          <Techo
            src="/logos/Git_icon.svg"
            name="Git"
            href="https://git-scm.com/"
          />
          <Techo
            src="/logos/full_colored_dark.svg"
            name="Bash"
            href="https://www.gnu.org/software/bash/"
          />
          <Techo name="Latex" href="https://www.latex-project.org/" />
        </CategoryUl>
      </div>
    </div>
  );
}
