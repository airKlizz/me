import { Title, SubTitle } from "./Utils/Title";

export default function Contact() {
  return (
    <div className="contact p-border rounded-theme-ext col-span-2 lg:col-span-4 lg:row-span-4 lg:aspect-square bg-gradient-to-br from-primary to-secondary">
      <div className="p-3 h-full w-full bg-bg/75 rounded-theme-int">
        <Title>Contact</Title>
        <p>
          If you want to contact me, please feel free to send me a DM on Twitter
          or send me an email.
        </p>
        <p>Email: remi.calizzano at gmail.com</p>
        <p>Looking forward to hear from you.</p>
      </div>
    </div>
  );
}
