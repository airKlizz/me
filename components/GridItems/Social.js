import Link from "next/link";
import Image from "next/image"

function Entity({src, alt, href, imgClassName}) {
  return (
    <div className="col-span-1 row-span-1 p-3 aspect-square">
      <a
        target="_blank" rel="noopener noreferrer"
        href={href}
      >
        <Image
          className={`${imgClassName != null ? imgClassName : ""}`}
          src={src}
          alt={alt}
          width={256}
          height={256}
        />
      </a>
    </div>
  )
}

export default function Social() {
  return (
    <div className="social p-border rounded-theme-ext lg:col-span-2 lg:row-span-2 aspect-square bg-gradient-to-br from-primary to-secondary">
      <div className="p-3 h-full rounded-theme-int grid grid-cols-2 grid-rows-2 bg-bg/75">
        <Entity src="/logos/Social_Icons_linkedin.svg.original.svg" alt="LinkedIn logo" href="https://www.linkedin.com/in/remi-calizzano/"/>
        <Entity src="/logos/Twitter-logo.svg" alt="Twitter logo" href="https://twitter.com/airklizz"/>
        <Entity src="/logos/gitlab-logo-500.svg" alt="GitLab logo" href="https://gitlab.com/airKlizz" imgClassName="scale-[1.8]"/>
        <Entity src="/logos/Octicons-mark-github.svg" alt="GitHub logo" href="https://github.com/airKlizz"/>
      </div>
    </div>
  );
}
