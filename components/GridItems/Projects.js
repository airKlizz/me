import { Title, SubTitle } from "./Utils/Title";
import { Tab } from "@headlessui/react";
import React, { useState, useEffect } from "react";
import Image from "next/image"

function getWindowWidth() {
  const { innerWidth: width } = window;
  return width;
}

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

function TabStyled({ children }) {
  return (
    <Tab
      className={({ selected }) =>
        classNames(
          "w-full font-medium px-3 focus:outline-none border-b-2 border-bg/50",
          selected ? "border-text/90" : "hover:border-text/75"
        )
      }
    >
      {children}
    </Tab>
  );
}

function TabPanelStyled({ children }) {
  return (
    <Tab.Panel
      className={classNames(
        "rounded-b-theme-int h-full bg-bg/50 p-5",
        "focus:outline-none"
      )}
    >
      {children}
    </Tab.Panel>
  );
}

function Project({ title, src, content, href }) {
  return (
    <a target="_blank" rel="noopener noreferrer" href={href}>
      <div className="grid grid-cols-5 gap-5 h-full w-full">
        <div className="pt-2">
          <Image
            src={src}
            alt={`${title} logo`}
            width={100}
            height={100}
          />
        </div>
        <div className="col-span-4">
          <SubTitle>{title}</SubTitle>
          <p>{content}</p>
        </div>
      </div>
    </a>
  );
}

function ProjectSmallScreen({ title, src, content, href }) {
  return (
    <a target="_blank" rel="noopener noreferrer" href={href}>
      <div className="grid grid-cols-5 gap-5">
        <div className="pt-2">
          <Image
            src={src}
            alt={`${title} logo`}
            width={100}
            height={100}
          />
        </div>
        <div className="col-span-4 my-auto py-0">
          <SubTitle>{title}</SubTitle>
        </div>
      </div>
      <p>{content}</p>
    </a>
  );
}

const projects = [
  {
    title: "Lizz",
    src: "/projects/lizz.png",
    href: "https://openlizz.com",
    content:
      "Lizz is a CLI to help the deployment of applications in a Kubernetes cluster following the GitOps principle. It is built on top of Flux. It uses variables to share information between applications and allows to code the configuration of the application depending on the cluster state.",
  },
  {
    title: "ELG SDK",
    src: "/projects/elg.svg",
    href: "https://gitlab.com/european-language-grid/platform/python-client",
    content:
      "The ELG Python SDK is a Python package that allows the users to use the ELG functionalities in Python by interacting with the different ELG APIs. With the ELG SDK, the users can call the services, search resources, download corpora, ... The ELG SDK has also its own functionalities like the possibility to create an ELG compatible service in Python, or deploy ELG services locally.",
  },
  {
    title: "MDMLS",
    src: "/projects/summarization.png",
    href: "https://github.com/airklizz/mdmls",
    content:
      'A Python package that performs automatic summarization of large texts in English, German, French, Spanish, Portuguese, Polish, and Italian. It uses the combination of extractive and abstractive summarizations, a method presented in my "Generating Extended and Multilingual Summaries with Pre-trained Transformers" paper.',
  },
  {
    title: "ELG Translate",
    src: "/projects/translation.png",
    href: "https://translate.european-language-grid.eu/",
    content:
      "A translation application powered by machine translation services from the ELG catalogue build with NextJS and FastAPI.",
  },
];

export default function Projects() {
  const [windowWidth, setWindowWidth] = useState();
  useEffect(() => {
    function handleResize() {
      setWindowWidth(getWindowWidth());
    }
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  if (windowWidth >= 768) {
    return (
      <div className="projects p-border rounded-theme-ext col-span-2 lg:col-span-8 lg:row-span-4 lg:aspect-[2/1] bg-gradient-to-br from-primary to-secondary">
        <div className="h-full rounded-theme-int bg-bg/75">
          <div className="pt-3 px-3">
            <Title>Projects</Title>
          </div>
          <div className="h-[calc(100%-3.25rem)]">
            <Tab.Group>
              <Tab.List className="flex h-8">
                {projects.map(function (project, i) {
                  return <TabStyled key={i}>{project.title}</TabStyled>;
                })}
              </Tab.List>
              <Tab.Panels className="h-[calc(100%-theme(space.8))]">
                {projects.map(function (project, i) {
                  return (
                    <TabPanelStyled key={i}>
                      <Project
                        key={i}
                        title={project.title}
                        src={project.src}
                        href={project.href}
                        content={project.content}
                      />
                    </TabPanelStyled>
                  );
                })}
              </Tab.Panels>
            </Tab.Group>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="projects p-border rounded-theme-ext col-span-2 lg:col-span-8 lg:row-span-4 lg:aspect-[2/1] bg-gradient-to-br from-primary to-secondary">
        <div className="h-full rounded-theme-int bg-bg/75">
          <div className="pt-3 px-3">
            <Title>Projects</Title>
          </div>
          <div className="flex flex-col gap-3 p-3">
            {projects.map(function (project, i) {
              return (
                <ProjectSmallScreen
                  key={i}
                  title={project.title}
                  src={project.src}
                  href={project.href}
                  content={project.content}
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
