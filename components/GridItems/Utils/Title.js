export function Title({ children }) {
  return <h2 className="text-xl font-semibold mb-3">{children}</h2>;
}

export function SubTitle({ children }) {
  return <h3 className="text-l font-medium mt-2 mb-1">{children}</h3>;
}
