export default function Header() {
  return (
    <div className="avatar p-border rounded-theme-ext lg:col-span-2 lg:row-span-2 aspect-square bg-gradient-to-br from-primary to-secondary">
      <div className="w-full h-full rounded-theme-int bg-cover bg-center bg-[url('/me.jpg')]"></div>
    </div>
  );
}
