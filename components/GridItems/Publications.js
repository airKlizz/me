import { Title, SubTitle } from "./Utils/Title";

function Paper({ title, authors, year, journal, href }) {
  return (
    <li className="grid grid-cols-8 gap-2">
      <div className="col-span-7">
        <a target="_blank" rel="noopener noreferrer" href={href} className="hover:underline hover:cursor-pointer"><p className="font-semibold">{title}</p></a>
        <p className="text-sm truncate">{authors}</p>
        <p className="text-sm truncate">{journal}</p>
      </div>
      <div>
        <p className="text-right">{year}</p>
      </div>
    </li>
  );
}

export default function Publications() {
  return (
    <div className="publications p-border rounded-theme-ext col-span-2 lg:col-span-8 lg:row-span-6 bg-gradient-to-br from-primary to-secondary">
      <div className="p-3 h-full w-full bg-bg/75 rounded-theme-int">
        <Title>Publications</Title>
        <ul className="space-y-4">
          <Paper
            title="Generating Extended and Multilingual Summaries with Pre-trained Transformers"
            authors="Rémi Calizzano, Malte Ostendorff, Qian Ruan, Georg Rehm"
            year="2022"
            journal="Proceedings of the 13th Conference on Language Resources and Evaluation (LREC 2022), pages 1640–1650"
            href="http://www.lrec-conf.org/proceedings/lrec2022/pdf/2022.lrec-1.175.pdf"
          />
          <Paper
            title="Towards Practical Semantic Interoperability in NLP Platforms"
            authors="Julian Moreno-Schneider, Remi Calizzano, Florian Kintzel, Georg Rehm, Dimitrios Galanis, Ian Roberts"
            year="2022"
            journal="Testing the Annotation Consistency of Hallidayan Transitivity Processes A Multi-variable Structural Approach Dong, Min; Liu, Xiaoyan; Fang, Alex Chengyu"
            href="https://sigsem.uvt.nl/isa18/ISA-18_22_Paper.pdf"
          />
          <Paper
            title="DFKI SLT at GermEval 2021: Multilingual Pre-training and Data Augmentation for the Classification of Toxicity in Social Media Comments"
            authors="Rémi Calizzano, Malte Ostendorff, Georg Rehm"
            year="2021"
            journal="Proceedings of the GermEval 2021 Shared Task on the Identification of Toxic, Engaging, and Fact-Claiming Comments"
            href="https://aclanthology.org/2021.germeval-1.4/"
          />
          <Paper
            title="Ordering sentences and paragraphs with pre-trained encoder-decoder transformers and pointer ensembles"
            authors="Rémi Calizzano, Malte Ostendorff, Georg Rehm"
            year="2021"
            journal="Proceedings of the 21st ACM Symposium on Document Engineering"
            href="https://dl.acm.org/doi/abs/10.1145/3469096.3469874"
          />
          <Paper
            title="European language grid: A joint platform for the european language technology community"
            authors="Georg Rehm, Stelios Piperidis, Kalina Bontcheva, Jan Hajic, Victoria Arranz, Andrejs Vasiļjevs, Gerhard Backfried, José Manuel Gómez-Pérez, Ulrich Germann, Rémi Calizzano, Nils Feldhus, Stefanie Hegele, Florian Kintzel, Katrin Marheinecke, Julian Moreno Schneider, Dimitrios Galanis, Penny Labropoulou, Miltos Deligiannis, Katerina Gkirtzou, Athanasia Kolovou, Dimitris Gkoumas, Leon Voukoutis, Ian Roberts, Jana Hamrlová, Dusan Varis, Lukáš Kačena, Khalid Choukri, Valérie Mapelli, Mickaël Rigault, Jūlija Meļņika, Miro Janosik, Katja Prinz, Andres Garcia-Silva, Cristian Berrio, Ondřej Klejch, Steve Renals"
            year="2021"
            journal="Proceedings of the 16th Conference of the European Chapter of the Association for Computational Linguistics: System Demonstrations"
            href="https://aclanthology.org/2021.eacl-demos.26/"
          />
          <Paper
            title="MuSeM: Detecting incongruent news headlines using mutual attentive semantic matching"
            authors="Rahul Mishra, Piyush Yadav, Remi Calizzano, Markus Leippold"
            year="2020"
            journal="2020 19th IEEE International Conference on Machine Learning and Applications (ICMLA)"
            href="https://ieeexplore.ieee.org/abstract/document/9356134/"
          />
        </ul>
      </div>
    </div>
  );
}
