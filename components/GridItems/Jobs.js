import { Title, SubTitle } from "./Utils/Title";

function Job({ children, year }) {
  return (
    <li className="grid grid-cols-8 gap-2">
      <div className="col-span-7">{children}</div>
      <div>
        <p className="text-right">{year}</p>
      </div>
    </li>
  );
}

export default function Jobs() {
  return (
    <div className="jobs p-border rounded-theme-ext col-span-2 lg:col-span-8 lg:row-span-6 bg-gradient-to-br from-primary to-secondary">
      <div className="p-3 h-full w-full bg-bg/75 rounded-theme-int">
        <Title>Jobs</Title>
        <a target="_blank" rel="noopener noreferrer" href="https://dfki.de/en/web" className="hover:underline">
          <SubTitle>
            German Research Centre for Artificial Intelligence (DFKI)
          </SubTitle>
        </a>
        <ul className="mt-4 space-y-4">
          <Job year="2022 2021">
            <p className="pl-3 font-semibold">Software Engineer</p>
            <p className="pl-3 mt-1">
              Working for the{" "}
              <a target="_blank" rel="noopener noreferrer" href="https://www.european-language-grid.eu/" className="hover:underline">
                European Language Grid project
              </a>{" "}
              as a junior researcher on various aspects of the project
              including:
            </p>
            <ul className="pl-3 list-inside list-disc">
              <li>Development of a Python SDK to interact with the ELG APIs</li>
              <li>
                Managing the ELG infrastructure (based on Kubernetes):
                monitoring, deployment of new ELG services, HA with
                replications, updates
              </li>
              <li>
                NLP research: multi-lingual summarisation and sentence ordering
              </li>
            </ul>
          </Job>
          <Job year="2020">
            <p className="pl-3 font-semibold">Junior Researcher</p>
            <p className="pl-3 mt-1">
              Internship in NLP research with the development of NLP tools for
              the{" "}
              <a target="_blank" rel="noopener noreferrer" href="https://qurator.ai/" className="hover:underline">
                QURATOR project
              </a>
              .
            </p>
            <p className="pl-3">
              Topics: Information Retrieval, Automatic summarisation, Semantic
              Storytelling.
            </p>
          </Job>
        </ul>
      </div>
    </div>
  );
}
